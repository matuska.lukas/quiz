/**
 * The admin router of the app
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 * @see https://lukasmatuska.cz/
 */

/**
 * Express router API
 */
const router = require('express').Router();

/**
 * Libraries
 */
const moment = require('moment');
const numberFormat = require('../libs/numberFormat');

/**
 * Controllers
 */
const errorController = require('../controllers/error');
const gameController = require('../controllers/game');
const pageController = require('../controllers/admin/page');
const userController = require('../controllers/user');

/**
 * Partials methods
 */
const partials = require('./partials');

/**
 * Routes
 */

// set local variables
router.all('/*', (req, res, next) => {
    res.locals = {
        currentPath: req.originalUrl,
        moment,
        numberFormat,
    };

    // move to the next route
    next();
});

// Redirect from /admin to the login page or to dashboard
router.get('/', partials.loggedIn, (req, res) => {
    res.redirect('/admin/dashboard');
});

// Display login page
router.get('/login', partials.loggedIn, (req, res) => {
    pageController.loginPage(req, res);
});

// post login page (do the login)
router.post('/login', partials.loggedIn, (req, res) => {
    userController.login(req, res);
});

/**
 * Register
 */
router.get('/register', partials.loggedIn, (req, res) => {
    pageController.registerPage(req, res);
});

router.post('/register', partials.loggedIn, (req, res) => {
    userController.registerMe(req, res);
});

router.get('/forgot-password', partials.loggedIn, (req, res) => {
    pageController.loginPage(req, res);
});

/**
 * This row block access without logging in
 */
router.all('/*', partials.loginControl);

// This row block access without admin permissions
router.all('/*', partials.filterNonAdmin);

/**
 * Dashboard
 */

router.get('/dashboard', (req, res) => {
    pageController.dashboard(req, res);
});

/**
 * Question sets
 */

router.get('/questionSets/add', (req, res) => {
    pageController.questionSetsAdd(req, res);
});

router.post('/questionSets/add', (req, res) => {
    gameController.addQuestionSet(req, res);
});

router.get('/questionSets/show', (req, res) => {
    pageController.questionSetsList(req, res);
});

router.get('/questionSets/detail', (req, res) => {
    pageController.questionSetDetail(req, res);
});

router.get('/questionSets/edit', (req, res) => {
    pageController.questionSetEdit(req, res);
});

router.post('/questionSets/edit', (req, res) => {
    gameController.editQuestionSet(req, res);
});


/**
 * Questions
 */

router.get('/questions/add', (req, res) => {
    pageController.questionsAdd(req, res);
});

router.post('/questions/add', (req, res) => {
    gameController.addQuestion(req, res);
});

/**
 * Users
 */
router.get('/users/new', (req, res) => {
    pageController.userNew(req, res);
});

router.get('/users/list', (req, res) => {
    pageController.userList(req, res);
});

router.get('/users-api/list', (req, res) => {
    userController.list((err, users) => {
        if (err) {
            res.send('err');
            return console.error(err);
        } else {
            return res.send(users);
        }
    });
});

router.post('/users-api/new/object', (req, res) => {
    userController.importFromObject(req, res);
});

/**
 * Settings
 */
/*router.get('/game/settings', (req, res) => {
    pageController.gameSettings(req, res);
});*/

router.post('/game/settings', (req, res) => {
    gameController.updateSettings(req, res);
});

/**
 * Logout
 */
router.get('/logout', (req, res) => {
    userController.logout(req, res);
});

/**
 * Not found the rerquested path
 */
router.all('*', (req, res) => {
    errorController.error404admin(req, res);
});

/**
 * Export the router
 */
module.exports = router;