/**
 * The entry point of the Quiz app
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 * @see https://lukasmatuska.cz/
 */

// config as global variable
global.CONFIG = require('./config');

// load the server plugin (Express)
const express = require('express');
const app = express();

// load some libraries
const moment = require('moment');
const path = require('path');
const bodyparser = require('body-parser');

// session handling
const session = require('express-session');
const redis = require('redis');
const redisStore = require('connect-redis')(session);

// connect to the redis server
const redisClient = redis.createClient();
const store = new redisStore({
    host: 'localhost', 
    port: 6379,
    client: redisClient,
    ttl: 86400,
});

// set up the redis store to saving session data
app.use(session({
    secret: 'Purples... the best ples.',
    store: store,
    name: 'BSID',
    resave: true,
    saveUninitialized: false,
    cookie: {
        maxAge: 86400000,
    },
}));

// set extended urlencoded to true (post)
app.use(bodyparser.urlencoded({extended: true}));

// set up views directory and the rendering engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// set serving static files from the static dir
app.use(express.static(path.join(__dirname, 'static')));

/**
 * Routers
 */
const admin = require('./routes/admin');
app.use('/admin', admin);

const main = require('./routes/main');
app.use('/', main);

/*
// error handling
app.use((req, res) => {
    res.status(400).send('Generic 400 error<br/>\n' + err);
});

app.use((err, req, res, next) => {
    res.status(500).send('Generic server error<br/>\n' + err);
});*/

// run the server
app.listen(CONFIG.port, () => {
    console.log(moment().format('YYYY-MM-DD HH:mm:ss') + ' Listening on port ' + CONFIG.port + ' (Quiz Node.js app)');
});