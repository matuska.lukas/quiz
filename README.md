# Quiz
## Installation

1. Clone the repo:
```bash
git clone git@gitlab.com:matuska.lukas/quiz.git
```
2. Install Node and MongoDB
- It depends on your system

3. Config MongoDB
- Create database and user
```mongo
use quiz
db.createUser({user: 'quiz', pwd: 'quiz', roles: [{role: 'readWrite', db: 'quiz'}]});
```

4. Get ready config file:
```bash
cp config.js.sample config.js
```
- Edit settings of your MongoDB database and user in your `config.js`
