/**
 * PURPLES. NEW.
 * The Purples website and system.
 * 
 * @description The starting file of the whole Purples website and administration
 * @author Jan Svabik
 * @version 3.0
 */

global.CONFIG = require('./config');

// load the server plugin (Express)
const express = require('express');
const app = express();

// load the libraries
const moment = require('moment');
const path = require('path');
const bodyparser = require('body-parser');

// session handling
const session = require('express-session');
const redis = require('redis');
const redisStore = require('connect-redis')(session);

// connect to the redis server
const redisClient = redis.createClient();
const store = new redisStore({
    host: 'localhost', 
    port: 6379,
    client: redisClient,
    ttl: 86400,
});

// set up the redis store to saving session data
app.use(session({
    secret: 'Purples... the best ples.',
    store: store,
    name: 'BSID',
    resave: true,
    saveUninitialized: false,
    cookie: {
        maxAge: 86400000,
    },
}));

// set extended urlencoded to true (post)
app.use(bodyparser.urlencoded({extended: true}));

// set up views directory and engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// redirect non-www version to the www version
app.all(/.*/, (req, res, next) => {
    if (req.headers.host.match(/^purples\.cz/))
        res.redirect(301, 'https://www.purples.cz' + req.url);
    else
        return next();
});

// allow serving static files from the static dir
app.use(express.static(path.join(__dirname, 'static')));

// routers
const admin = require('./routers/admin');
const web = require('./routers/web');
app.use('/admin', admin);
app.use('/', web);

// error handling
app.use((req, res) => {
    res.status(400).send('Generic 400 error\n' + err);
});

app.use((err, req, res, next) => {
    res.status(500).send('Generic server error\n' + err);
});

// run the server
app.listen(3015, () => {
    console.log(moment().format('YYYY-MM-DD HH:mm:ss') + ' Listening on port 3015 (Purples 3.0)');
});
