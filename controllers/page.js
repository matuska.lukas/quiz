/**
 * Page controller
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 *  Libs
 */


/**
 * Models
 */

// display the login page
module.exports.loginPage = (req, res) => {
    let randomLove = ['my password', 'Purkyňka', 'Purples', 'wannacry', '"slivovice"', 'rum', 'vodka', 'music', 'sex', 'money', 'you', '"maturita"', 'ice cream', 'Nutella', 'math', 'Xmas', 'coffe', 'my life', 'winter', 'summer', 'bananas', 'flying carpet', 'teas', 'turnstiles', 'scream', 'slavs'];

    res.render('login', {
        randomLove: randomLove[Math.floor(Math.random() * randomLove.length)],
        req: req,
        res: res
    });
};

// display the register page
module.exports.registerPage = (req, res) => {
    let randomLove = ['my password', 'Purkyňka', 'Purples', 'wannacry', '"slivovice"', 'rum', 'vodka', 'music', 'sex', 'money', 'you', '"maturita"', 'ice cream', 'Nutella', 'math', 'Xmas', 'coffe', 'my life', 'winter', 'summer', 'bananas', 'flying carpet', 'teas', 'turnstiles', 'scream', 'slavs'];

    res.render('register', {
        randomLove: randomLove[Math.floor(Math.random() * randomLove.length)],
        req: req,
        res: res
    });
};