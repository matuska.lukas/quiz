const bcrypt = require('bcrypt');
const moment = require('moment');
moment.locale('cs');
const nodemailer = require('nodemailer');

// models
const User = require('../models/User');
const QuestionSet = require('../models/QuestionSet');

// display the register page
module.exports.viewRegisterPage = (req, res) => {
    let randomLove = ['my password', 'Purkyňka', 'Purples', 'wannacry', '"slivovice"', 'rum', 'vodka', 'music', 'sex', 'money', 'you', '"maturita"', 'ice cream', 'Nutella', 'math', 'Xmas', 'coffe', 'my life', 'winter', 'summer', 'bananas', 'flying carpet', 'teas', 'pizza "Baníček"', 'turnstiles', 'scream', 'slavs'];

    return res.render('register', {
        randomLove: randomLove[Math.floor(Math.random() * randomLove.length)],
    });
};

// do the register
module.exports.registerMe = (req, res) => {
    let email = req.body.email.trim().toLowerCase();
    let username = req.body.username.trim().toLowerCase();
    User.findOne({
        $or: [
            {
                email: email,
            },
            {
                username: username,
            }
        ],
    }, (err, user) => {
        if (err)
            return console.error(err);
        if (user && user.username === username) {
            return res.redirect('/register/?err=usernameAlreadyExists');
        }
        // 
        if (user && user.email === email) {
            return res.redirect('/register/?err=emailAlreadyExists');
        }

        new User({
            name: {
                first: req.body.firstname,
                last: req.body.lastname,
            },
            username: req.body.username,
            password: bcrypt.hashSync(req.body.password, 15), // don't go under 13!
            email: req.body.email,
        }).save((err, user) => {
            if (err) {
                return console.error(err);
            }

            // Create a SMTP transporter object
            let transporter = nodemailer.createTransport(global.CONFIG.nodemailer.settings);

            // Message object
            let message = {
                from: `${CONFIG.nodemailer.senderInfo.name} <${CONFIG.nodemailer.senderInfo.email}>`,
                to: user.name.first + ' ' + user.name.last + ' <' + user.email + '>',
                subject: 'Welcome to quiz',
                text: `Welcome,\n\nhttps://${CONFIG.myDomain}/login\n\nBest regards,\nQuiz admin team`,
                //html: '<h1>Dobrý den,</h1><p>zde je vaše heslo <b>' + newPassword + '</b>!</p><p>Nyní se můžete přihlásit s tímto heslem.</p><br><p>S přáním hezkého dne,<br/>Pořadatelé Pohádkového lesa v Rudici</p>'
                //html: fs.readFileSync('../mail/forgot-password.html');
            };

            transporter.sendMail(message, (err, info, response) => {
                if (err) {
                    console.log('Error occurred. ' + err.message);
                    return process.exit(1);
                }
                //console.log(info);
                //console.log(response);
            });

            QuestionSet.updateOne({
                _id: CONFIG.defaultQSId,
            }, {
                    $push: {
                        users: user._id,
                    },
                }, (err) => {
                    if (err) {
                        return console.error(err);
                    }
                    req.session.user = user;
                    res.redirect('/game');
                });
        });
    });
};

// display the login page
module.exports.viewLoginPage = (req, res) => {
    let randomLove = ['palačinky', 'Purkyňku', 'maliny', 'jahody', 'sebe', 'Purplesy', 'VUT', 'tombolu', 'chobotnice', 'slivovici', 'rum', 'vodku', 'hudbu', 'vstávání', 'sex', 'peníze', 'tě', 'maturitu', 'plesy', 'zmrzlinu', 'marmeládu', 'Nutellu', 'matematiku', 'Vánoce', 'Velikonoce', 'kávu', 'život', 'ticho', 'umění', 'zimu', 'jaro', 'podzim', 'léto', 'chleba', 'rohlíky', 'okurky', 'banány', 'lítající koberce', 'čaje', 'pizzu Baníček', 'Xenii', 'turnikety'];

    res.render('login', {
        randomLove: randomLove[Math.floor(Math.random() * randomLove.length)],
    });
};

// do the login
module.exports.login = (req, res) => {
    User.findOne({
        username: req.body.username.trim().toLowerCase(),
    }, (err, user) => {
        if (err)
            return console.error(err);

        if (!user) {
            return res.redirect('/login?err=not-existing-user');
        }

        // compare the passwords
        bcrypt.compare(req.body.password, user.password, (err, same) => {
            if (err)
                return console.error(err);

            // if the password are not the same, return message
            if (!same)
                return res.redirect('/login?err=badPassword');

            req.session.user = user;
            if (user.admin) {
                return res.redirect('/admin');
            }
            res.redirect('/game');
        });
    });
};

// logout the user
module.exports.logout = (req, res) => {
    req.session.destroy();
    res.redirect('/login/?info=logout-ok');
};