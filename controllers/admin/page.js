/**
 * Page controller
 * @author Oliver Slapal
 * @version 1.0
 */

/**
 * Libs
 */

/**
 * Models
 */
const Question = require('../../models/Question');
const QuestionSet = require('../../models/QuestionSet');

module.exports.dashboard = (req, res) => {
    return res.render('admin/dashboard', { req, res, title: 'Dashboard', active: 'dashboard' });
};

// display the login page
module.exports.loginPage = (req, res) => {
    let randomLove = ['my password', 'Purkyňka', 'Purples', 'wannacry', '"slivovice"', 'rum', 'vodka', 'music', 'sex', 'money', 'you', '"maturita"', 'ice cream', 'Nutella', 'math', 'Xmas', 'coffe', 'my life', 'winter', 'summer', 'bananas', 'flying carpet', 'teas', 'turnstiles', 'scream', 'slavs'];

    res.render('admin/login/login', {
        randomLove: randomLove[Math.floor(Math.random() * randomLove.length)], req, res
    });
};

// display the register page
module.exports.registerPage = (req, res) => {
    let randomLove = ['hody', 'slivovicu', 'vodku', 'muziku'/*, 'sex'*/, 'peníze', 'sebe', 'tě', 'maturitu', 'zmrzlinu', 'Nutellu', 'matiku', 'Vánoce', 'kafe', 'svůj život', 'zimu', 'léto', 'banány', 'čaje', 'pizzu "Gladiátor"'/*, 'píču'*/];

    res.render('admin/login/register', {
        randomLove: randomLove[Math.floor(Math.random() * randomLove.length)], req, res
    });
};

// display the forgot password page
module.exports.forgotPasswordPage = (req, res) => {
    let randomLove = ['hody', 'slivovicu', 'vodku', 'muziku'/*, 'sex'*/, 'peníze', 'sebe', 'tě', 'maturitu', 'zmrzlinu', 'Nutellu', 'matiku', 'Vánoce', 'kafe', 'svůj život', 'zimu', 'léto', 'banány', 'čaje', 'pizzu "Gladiátor"'/*, 'píču'*/];

    res.render('admin/login/forgotPassword', {
        randomLove: randomLove[Math.floor(Math.random() * randomLove.length)], req, res
    });
};

/**
 * Question sets
 */
module.exports.questionSetsAdd = (req, res) => {
    res.render('admin/questionSets/add', { req, res });
};

module.exports.questionSetsList = (req, res) => {
    QuestionSet.find({}, (err, questionSets) => {
        if (err) {
            res.send('err');
            return console.error(err);
        }
        return res.render('admin/questionSets/list', { req, res, title: 'Show question sets', active: 'questionSets/show', questionSets });
    });
};

module.exports.questionSetEdit = (req, res) => {
    QuestionSet.find({
        _id: req.query.id,
    }, (err, questionSets) => {
        if (err) {
            res.send('err');
            return console.error(err);
        }
        return res.render('admin/questionSets/edit', { req, res, title: `Edit question set '${questionSets[0].name}'`, active: 'questionSets/edit', questionSet: questionSets[0] });
    });
};

module.exports.questionSetDetail = (req, res) => {
    QuestionSet.find({
        _id: req.query.id,
    }, (err, questionSets) => {
        if (err) {
            res.send('err');
            return console.error(err);
        }
        return res.render('admin/questionSets/detail', { req, res, title: `Detail of question set '${questionSets[0].name}'`, active: 'questionSets/detail', questionSet: questionSets[0] });
    });
};

/**
 * Questions
 */
module.exports.questionsAdd = (req, res) => {
    res.render('admin/questions/add', { req, res });
};

module.exports.gameSettings = (req, res) => {
    res.render('admin/game/settings', { req, res });
};

/*module.exports.listForms = (req, res) => {
    res.render('user-site/forms/list', { req, res, title: 'Forms table', active: 'forms-list', forms: [] });
};

module.exports.newForm = (req, res) => {
    res.render('user-site/forms/new', { req, res, title: 'New form', active: 'forms-new' });
};*/

/**
 * Users
 */
module.exports.userNew = (req, res) => {
    return res.render('admin/users/new', { req, res, title: `New user`, active: 'users/new' });
};

module.exports.userList = (req, res) => {
    return res.render('admin/users/list', { req, res, title: 'List of users', active: 'users/list' });
};