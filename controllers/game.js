/**
 * Game controller
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */
const moment = require('moment');

/**
 * Models
 */
const Answer = require('../models/Answer');
const User = require('../models/User');
const Question = require('../models/Question');
const QuestionSet = require('../models/QuestionSet');

module.exports.start = (req, res) => {
    QuestionSet.find({
        users: req.session.user._id,
    }, (err, questionSets) => {
        if (err) {
            return console.error(err);
        }

        res.render('game', {
            questionSets: questionSets,
            req: req,
            res: res,
        });
    });
};

module.exports.selectQuestionSet = (req, res) => {
    if (!req.body.idOfQuestionSet) {
        return res.send('not-send-id');
    } else {
        QuestionSet.findOne({
            _id: req.body.idOfQuestionSet,
        }, (err, questionSet) => {
            if (err) {
                res.send(err);
                return console.error(err);
            }

            if (!questionSet) {
                res.send('bad-id-of-qs');
                return console.error(err);
            }

            Answer.countDocuments({
                author: req.session.user._id,
                questionSetId: req.body.idOfQuestionSet,
            }, (err, countOfAnswers) => {
                if (err) {
                    res.send(err);
                    return console.error(err);
                }

                if (countOfAnswers >= (questionSet.questions.length * questionSet.maxCountOfTries)) {
                    return res.send('reached-max-count-of-tries');
                }

                req.session.questionSet = questionSet;
                req.session.questionSet.startTime = new Date();
                req.session.idOfLastQuestion = -1;
                if (req.session.questionSet) {
                    return res.send('ok');
                } else {
                    return res.send('wtf');
                }
            });
        });
    }
};

module.exports.getQuestion = (req, res) => {
    console.log(req.session);
    if (req.session.questionSet) {
        if (req.session.idOfLastQuestion === undefined) {
            req.session.idOfLastQuestion = -1;
        }

        req.session.idOfLastQuestion += 1;

        if (req.session.idOfLastQuestion > req.session.questionSet.questions.length - 1) {
            req.session.idOfLastQuestion = -1;
            return res.send('end-of-qs');
        }

        let question = JSON.parse(JSON.stringify(req.session.questionSet.questions[req.session.idOfLastQuestion]));

        for (let i = 0; i < question.answers.length; i++) {
            delete question.answers[i].correct;
        }

        return res.send(question);
    } else {
        return res.send('req.session.questionSet-missing');
    }
};

module.exports.answerTheQuestion = (req, res) => {
    /*console.log(req.session.questionSet);
    console.log(req.session.idOfLastQuestion);
    console.log(req.body.idOfAnswer);*/
    if (req.session.questionSet && typeof req.session.idOfLastQuestion != 'undefined' && req.body.idOfAnswer) {
        // save answer to DB
        console.log(`question: ${req.session.idOfLastQuestion}`);
        console.log(`questions:`);
        console.log(req.session.questionSet.questions);
        console.log(`question: ${req.session.questionSet.questions[req.session.idOfLastQuestion]._id}`);
        console.log({
            author: req.session.user._id,
            answerId: req.body.idOfAnswer,
            questionSetId: req.session.questionSet._id,
            questionId: req.session.questionSet.questions[req.session.idOfLastQuestion]._id,
        });
        new Answer({
            author: req.session.user._id,
            answerId: req.body.idOfAnswer,
            questionSetId: req.session.questionSet._id,
            questionId: req.session.questionSet.questions[req.session.idOfLastQuestion]._id,
        }).save((err, answer) => {
            if (err) {
                res.send(JSON.stringify(err));
                return console.error(err);
            }
            //console.log(answer);
            return res.send('ok');
        });
    } else {
        if (!req.session.questionSet) {
            return res.send('not-set-questionSet');
        } else if (typeof req.session.idOfLastQuestion == 'undefined') {
            return res.send('not-set-idOfQuestion');
        } else if (!req.body.idOfAnswer) {
            return res.send('not-send-idOfAnswer');
        }
        return res.send('are-you-sure-about-that');
    }
};

module.exports.getResults = (req, res) => {
    let results = {};
    Answer.find({
        author: req.session.user._id,
        questionSetId: req.session.questionSet._id,
        date: {
            $lt: new Date(),
            $gte: new Date(new Date().setDate(new Date().getDate() - 1)),
            //$gte: new Date(req.session.questionSet.startTime),
            //$gte: new Date(new Date().setDate(req.session.questionSet.startTime)),
        },
    }, (err, answers) => {
        //console.log(answers);
        if (err) {
            res.send(err);
            return console.error(err);
        }
        if (answers === undefined) {
            return res.send('not-found');
        }
        let score = 0;
        let maxTime = 0;
        for (let i = 0; i < answers.length; i++) {
            let ahoj = true;
            for (let ii = 0; ii < req.session.questionSet.questions.length; ii++) {
                if (ahoj) {
                    maxTime += req.session.questionSet.questions[ii].timeToAnswer;
                    ahoj = false;
                }
                for (let iii = 0; iii < req.session.questionSet.questions[ii].answers.length; iii++) {
                    const answerInQuestion = req.session.questionSet.questions[ii].answers[iii];
                    if (answerInQuestion.correct) {
                        score += 10;
                    }
                }
            }
        }
        //console.log(maxTime);
        //console.log(moment(req.session.questionSet.startTime).diff(moment(answers[answers.length - 1].date), 'seconds'));
        results.username = req.session.user.username;
        //results.score = (score / moment(req.session.start).diff(moment(answers[0].date), 'seconds') * 100);
        results.score = ((1 - (moment(req.session.start).diff(moment(answers[0].date), 'seconds') - maxTime)) * score);
        /**
         * (1 - (consumedTime - maxTime))*score 
         */

        //console.log(`${req.session.user.name.first} ${req.session.user.name.last}: ${results.score}`);

        results.try = (req.session.questionSet.maxCountOfTries - (req.session.questionSet.questions.length / ((req.session.questionSet.questions.length * req.session.questionSet.maxCountOfTries) / (answers.length))));
        results.maxCountOfTries = req.session.questionSet.maxCountOfTries;
        return res.send(results);
    });
};

module.exports.addQuestionSet = (req, res) => {
    // In future create questions from POST :)
    new QuestionSet(
        {
            name: req.body.name,
            admins: [
                req.session.user._id,
            ],
            users: [
                req.session.user._id,
            ],
            questions: [
                {
                    wording: "Jsi třeťák?",
                    timeToAnswer: 60,
                    answers:
                        [
                            {
                                text: 'Ano',
                                correct: true,
                            },
                            {
                                text: 'Ne',
                                correct: false,
                            },
                        ],
                }, {
                    wording: 'Máš data ve složce "/home2"?',
                    timeToAnswer: 20,
                    answers:
                        [
                            {
                                text: 'To moc dobře víte!',
                                correct: true,
                            }, {
                                text: 'Asio',
                                correct: true,
                            }, {
                                text: 'Asine',
                                correct: false
                            }, {
                                text: 'Ne',
                                correct: false,
                            },
                        ],
                }, {
                    wording: 'RIP',
                    timeToAnswer: 25,
                    answers:
                        [
                            {
                                text: 'Směrovací prokotoul',
                                correct: true,
                            }, {
                                text: 'Sra*ka',
                                correct: true,
                            },
                        ],
                },
            ],
        }
    ).save((err, questionSet) => {
        if (err) {
            return console.error(err);
        }
        res.render('admin/questionSets/add', { questionSet, req, res });
        // res.redirect('/game');
    });
};

module.exports.editQuestionSet = (req, res) => {
    if (req.body.id === undefined) {
        return res.send('not-sent-id');
    }
    if (req.body.questions != undefined) {
        var questions = req.body.questions;
        QuestionSet.updateOne({
            _id: req.body.id,
        }, {
            questions,
        }, (err) => {
            if (err) {
                res.send('err');
                return console.error(err);
            }
            return res.send('ok');
        });
    } else {
        return res.send('not-sent-questions');
    }
};

module.exports.addQuestion = (req, res) => {
    new Question({
        wording: req.body.wording,
        answers: req.body.answers,
        timeToAnswer: req.body.timeToAnswer,
    }).save((err, question) => {
        if (err) {
            return console.error(err);
        }
        res.render('admin/questions/add', { question, req, res });
        // res.redirect('/game');
    });
};

module.exports.showQuestion = (req, res, idOfQuestionSet, idOfQuestion) => {
    QuestionSet.findOne({
        _id: idOfQuestionSet,
    }, (err, questionSet) => {
        if (err) {
            return console.error(err);
        }

        if (!req.session.user._id in questionSet.users) {
            res.redirect('/game/');
        } else {
            if (idOfQuestion != questionSet.questions.length) {
                res.render(
                    'game/showQuestion',
                    {
                        question: questionSet.questions[idOfQuestion],
                        idOfQuestion, req, res,
                    }
                );
            } else {
                res.render('game/finalScore', { req, res, });
            }
        }
    });
};

module.exports.save = (req, res, questionSet, idOfQuestion, idOfAnswer) => {
    // here i save answers to MongoDB
    let userId = req.session.user._id;
    console.log(questionSet.questions.find(question => question._id == idOfQuestion).answers.find(answer => answer._id == idOfAnswer));
    //console.log('qs: ' + questionSet);
    //console.log('idOfAnswer: ' + idOfAnswer);
    //console.log(questionSet.questions[idOfQuestion].answers[idOfAnswer]);
    new Answer({
        userId: userId,
        questionSetId: questionSet._id,
        questionId: idOfQuestion,
        date: moment()/*.format('D. M. YYYY, HH:mm:ss')*/,
        answer: questionSet.questions.find(question => question._id == idOfQuestion).answers.find(answer => answer._id == idOfAnswer),
        /*
        answer: {
            //text: questionSet.questions[idOfQuestion].answers[idOfAnswer].text,
            text: questionSet.questions.find(question => question._id == idOfQuestion).answers.find(answer => answer._id == idOfAnswer).text,
            //correct: questionSet.questions[idOfQuestion].answers[idOfAnswer].correct,
            correct: questionSet.questions.find(question => question._id == idOfQuestion).answers.find(answer => answer._id == idOfAnswer).correct,
        },*/
    }).save((err, answer) => {
        if (err) {
            return console.error(err);
        }
    })
};