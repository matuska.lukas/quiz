/**
 * Error controller
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

 module.exports.error403 = (req, res) => {
    return res.render('error/accessDenied', {req, res});
 };
 
 module.exports.error404 = (req, res) => {
    return res.render('error/notFound', {req, res});
 };

 module.exports.error403admin = (req, res) => {
    return res.render('admin/accessDenied', {req, res});
 };
 
 module.exports.error404admin = (req, res) => {
    return res.render('admin/notFound', {req, res});
 };

 module.exports.badReCaptcha = (req, res) => {
    return res.render('badReCaptcha', {req, res});
 };