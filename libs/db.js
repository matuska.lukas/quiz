const mongoose = require('mongoose');
const moment = require('moment');

// connect to the database
mongoose.connect('mongodb://' + CONFIG.db.host + '/' + CONFIG.db.name, {
	auth: {
		authdb: CONFIG.db.name,
	},
	port: CONFIG.db.port,
	user: CONFIG.db.user,
	pass: CONFIG.db.password,
	useNewUrlParser: true,
}, (err) => {
    if (err)
    	return console.error(`Mongoose connection error: ${err}`);

    console.log(moment().format('YYYY-MM-DD HH:mm:ss') + ' Connected to database');
});

module.exports = mongoose;