/**
 * Answer database model
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

// library for easy database manipulations
const mongoose = require('../components/db');
const moment = require('moment');

// the schema itself
var answerSchema = new mongoose.Schema({
    author: String,
    questionSetId: String,
    questionId: String,
    answerId: String,
    date: {
        type: Date,
        default: moment(),
    },
});

// export
module.exports = mongoose.model('Answer', answerSchema, 'answer');