/**
 * Settings database model
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

// library for easy database manipulations
 const mongoose = require('../components/db');

// the schema itself
var settingsSchema = new mongoose.Schema({
    idOfActiveQuestionSet: String,
    timeToAnswer: Number,
});


// export
module.exports = mongoose.model('Settings', settingsSchema, 'settings');