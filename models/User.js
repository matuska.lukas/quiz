/**
 * User database model
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

/**
 * Libs
 */
// library for easy database manipulations
const mongoose = require('../components/db');
const moment = require('moment');
//moment.locale('cs');

// the schema itself
var userSchema = new mongoose.Schema({
    name: {
        first: String,
        last: String,
    },
    username: String,
    password: String,
    rescuePassword: String,
    email: String,
    photoUrl: {
        type: String,
        default: "/images/users/default.jpg",
    },
    author: {
        type: String,
    },
    permissions: [
        {
            type: String,
            //enum: CONFIG.
        }
    ],
    logins: [
        {
            time: {
                type: Date,
                default: moment(),
            },
            ip: String,
        },
    ],
});

// export
module.exports = mongoose.model('User', userSchema, 'user');