/**
 * Question database model
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

// library for easy database manipulations
 const mongoose = require('../components/db');

// the schema itself
var questionSchema = new mongoose.Schema({
    wording: String,
    answers: [{
        text: String,
        correct: Boolean,
    }],
    timeToAnswer: {
        type: Number,
        default: 10,
    },
});


// export
module.exports = mongoose.model('Question', questionSchema, 'question');