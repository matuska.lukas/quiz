/**
 * QuestionSet database model
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 */

// library for easy database manipulations
const mongoose = require('../components/db');

// the schema itself
const QuestionSetSchema = new mongoose.Schema({
    name: String,
    questions: [{
        wording: {
            type: String,
            default: "Default wording of question",
        },
        timeToAnswer: {
            type: Number,
            default: 10,
        },
        answers: [{
            text: String,
            correct: Boolean,
        }],
    }],
    maxCountOfTries: {
        type: Number,
        default: 3,
    },
    users: [String],
    admins: [String],
});


// export
module.exports = mongoose.model('QuestionSet', QuestionSetSchema, 'questionSet');