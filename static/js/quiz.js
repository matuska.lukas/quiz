var userAnswers = "";
var actualQuestion;

function selectQuestionSet() {
    $.post({
        url: '/game/select-question-set',
        data: {
            idOfQuestionSet: $('#selectQuestionSet').val(),
        },
        success: function (response) {
            //console.log(response);
            switch (response) {
                case 'ok':
                    console.log('QuestionSet selected successfully!');
                    startQuiz();
                    break;

                case 'reached-max-count-of-tries':
                    swal({
                        title: `You're out of tries!`,
                        text: 'Do you like our game?',
                        icon: 'warning',
                        buttons: {
                            cancel: {
                                text: "No",
                                value: false,
                                visible: true,
                                className: "",
                                closeModal: true,
                            },
                            confirm: {
                                text: "Yes!",
                                value: true,
                                visible: true,
                                className: "",
                                closeModal: true
                            },
                        },
                    });
                    break;

                default:
                    console.log('QuestionSet didn\'t selected successfully!');
                    swal({
                        title: 'Something went wrong!',
                        text: `Server said: "${response}"`,
                        icon: 'error',
                    });
                    break;
            }
        }
    });
}

function startQuiz() {
    let tmp = `<div id="gameContent">
  <img id="logoGame" src="/images/quiz.svg">
  <h1 id="gameh">question.wording</h1>
  <div id="countdownCon">
    <h2><i class="far fa-clock"></i>Time to answer:</h2><p id='countdown'></p>
  </div>
  <div id="answers"></div>
</div>`;
    $('#playingCourt').html(tmp);
    showQuestion();
    var actualQuestion;
}

function stopQuiz() {
    let tmp = `<div id="results" class='endBox'>
        <img id="logoResults" src="/images/quiz.svg">
        <p id="resultsh">Results</p>
        <div id="resultCon" class='mui-panel'>
            <div id="dataCon">
                <p class="resultHeadline"><i class="far fa-user"></i>Nick</p>
                <p class="resultData" id='usernameForResults'>your_username</p>
            </div>
            <div id="dataCon">
                <p class="resultHeadline"><i class="far fa-star"></i>Score</p>
                <p class="resultData" id='score'>your_score</p>
            </div>
            <div id="dataCon">
                <p class="resultHeadline"><i class="fas fa-history"></i>Remaining tries</p>
                <p class="resultData">
                    <span id='try'>actual_try</span> of <span id='maxCountOfTries'>0</span>
                </p>
            </div>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="button" style="display: none; " onclick="window.location.href=window.location.href" id="reloadButton">Try again</button>
    </div>`;
    $('#playingCourt').html($("<div>", { id: 'results', class: 'endBox' }).append(
        $("<img>", { id: 'logoResults', src: '/images/quiz.svg' }),
        $("<p>", { id: 'resultsh' }).text('Results'),
        $("<div>", { id: 'resultCon', class: 'mui-panel' }).append(
            $("<div>", { id: 'dataCon' }).append(
                $("<p>", { class: 'resultHeadline' }).append(
                    $("<i>", { class: 'far fa-user' }),
                    'Nick',
                ),
                $("<p>", { class: 'resultData', id: 'usernameForResults' }).text('your_username'),
            ),
            $("<div>", { id: 'dataCon' }).append(
                $("<p>", { class: 'resultHeadline' }).append(
                    $("<i>", { class: 'far fa-star' }),
                    'Score'
                ),
                $("<p>", { class: 'resultData', id: 'score' }).text('your_score'),
            ),
            $("<div>", { id: 'dataCon' }).append(
                $("<p>", { class: 'resultHeadline' }).append(
                    $("<i>", { class: 'fas fa-history' }),
                    'Remaining tries'
                ),
                $("<p>", { class: 'resultData' }).append(
                    $("<span>", { id: 'try' }).text('actual_try'),
                    ' of ',
                    $("<span>", { id: 'maxCountOfTries' }).text('maxCountOfTries'),
                ),
            ),
            $("<div>", { id: 'dataCon' }).append(
                $("<button>", {
                    type: 'button', class: 'btn btn-lg btn-primary btn-block', onclick: 'window.location.href=window.location.href', id: 'reloadButton', style: `border-radius: 7px;
                margin: auto auto;
                border: none;
                padding: 20px 25px;
                width: 250px;
                background-color: #374f7a;
                color: white;
                font-size: 20px;
                transition: .2s;
                cursor: pointer;` }).text('Try again'),
            ),
        ),
    ));
    // $('#playingCourt').html(tmp);
    $.get({
        url: '/game/get-results',
        data: {
            //any
        },
        success: function (response) {
            if (results.score == null) {
                results.score = 0;
            }
            //console.log(response);
            $('#usernameForResults').html(response.username);
            $('#score').html(response.score);
            $('#try').html(response.try);
            $('#maxCountOfTries').html(response.maxCountOfTries);
            if (response.maxCountOfTries > 0) {
                $('#reloadButton').show();
            }
        }
    });
}

function showQuestion() {
    $.get({
        url: '/game/get-question',
        data: {
            //any
        },
        success: function (response) {
            //console.log(response);
            switch (response) {
                case 'end-of-qs':
                    stopCountdown();
                    stopQuiz();
                    break;

                default:
                    actualQuestion = response;
                    $('#countdown').html(response.timeToAnswer);
                    $('#gameh').html(response.wording);
                    let answerButtons = ``;
                    for (let i = 0; i < response.answers.length; i++) {
                        answerButtons += (`<button class='inGameButton btn btn-primary btn--raised' value="${response.answers[i]._id}" id='${i}' onClick='answerTheQuestion(this.value);'>${response.answers[i].text}</button>`);
                    }
                    $('#answers').html(answerButtons);
                    progressCountdown();
                    console.log(actualQuestion);
                    break;
            }
        }
    });
}

function answerTheQuestion(idOfAnswer) {
    stopCountdown();
    $.post({
        url: '/game/answer-the-question',
        data: {
            //idOfQuestion: actualQuestion._id,
            idOfAnswer,
        },
        success: function (response) {
            //console.log(response);
            switch (response) {
                case 'ok':
                    //console.log(actualQuestion);
                    console.log('Successfully sent answer to server');
                    showQuestion();
                    break;

                default:
                    swal({
                        title: 'Something went wrong!',
                        text: response,
                        icon: 'error',
                    });
                    break;
            }
        }
    });
}

var check = true;
var timerId;

function progressCountdown() {
    var timeleft = $("#countdown").html() - 1;
    //var timeleft = document.getElementById("countdown").innerHTML - 1;
    var timerId = setInterval(countdown, 1000);

    function countdown() {
        if (navigator.onLine) {
            if (check) {
                if (timeleft == -1) {
                    clearTimeout(timerId);
                    end();
                    check = true;
                } else {
                    $("#countdown").html(timeleft);
                    //document.getElementById("countdown").innerHTML = timeleft;
                    timeleft--;
                }
            } else {
                clearTimeout(timerId);
                check = true;
            }
        } else {
            //alert('offline');
            swal({
                title: `You're offline!`,
                text: `Check your internet connection.`,
                icon: `error`,
            });
        }
    }

}

function stopCountdown() {
    check = false;
    console.log(`Answer time: ${$('#countdown').html()}`);
}

function end() {
    console.log('Your time has run out!');
    //document.getElementById('NaN').click();
    answerTheQuestion(0);
}