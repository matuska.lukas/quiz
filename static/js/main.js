var userAnswers = "";

function getNewQuestionFromQuestionSet(str, username="Player") {
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("txtHint").innerHTML = this.responseText;
            }
        };
        xmlhttp.open("GET","controlDatabase/showQuestion.php?indexOfQuestion="+str+"&name="+username,true);
        xmlhttp.send();
    }

function saveUserAnswers(userAnswer) {
    var date = new Date();
    var ms = date.getTime();
    if (userAnswer != 0) {
        userAnswers += userAnswer + "," + ms + ",";
    }else{
  	     userAnswers += ms + ",";
  }
}  

function saveAnswersToDatabase(username="Player") {
    if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("txtHint").innerHTML = this.responseText;
            }
        };
        xmlhttp.open("GET","controlDatabase/saveUser.php?userAnswers=" + username + "," + userAnswers, true);
    xmlhttp.send();
}

var check = true;
var timerId;

function progressCountdown(timeleft) {
    var elem = document.getElementById('countdown');
    var timerId = setInterval(countdown, 1000);

    function countdown() {
        if(navigator.onLine){
            if (check) {
                if (timeleft == -1) {
                    clearTimeout(timerId);    
                    end();
                    check = true;
                } else {
                    if (timeleft!=10) {
                        document.getElementById("countdown").innerHTML = timeleft;
                    }
                    timeleft--;
                }
            } else{
                clearTimeout(timerId);
                check = true;
            }
        } else {
                alert('offline');
        }
    }

}

function stopCountdown() {
    check = false;
}   

function end(){
    document.getElementById('NaN').click();
}
